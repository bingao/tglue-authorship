= More on the Criteria for Joint Authorship of Research Software
Bin Gao <bin.gao@uit.no>
v0.1.0, 2019-01-11
:toc: left
:sectnums:
:xrefstyle: full

== License

Copyright (C) 2018, 2019 Bin Gao.

Use of this document is granted under the terms of the Creative Commons
Attribution-ShareAlike 4.0 International License (CC BY-SA 4.0).

You should have received a copy of the license along with this work. If not,
see http://creativecommons.org/licenses/by-sa/4.0/.

== Rationale

In the field of computaion-oriented research and studies, development of the
corresponding software is usually the first step before making a scientific
publication <<Chestek2017>>. The identification of authors of a research
software can be important especially when all authors of the software are
individuals, as they may also be the copyright holders of the software if the
work is not *made for hire*.

The criteria for authorship of a research software can be different from those
for authorship of a journal (see for example the ICMJE authorship criteria
<<ICMJE2017>>). For instance, there are various cases that even an individual
does not have a single line of code in a research software, he/she can still
make considerable *intellectual work* to the software in many other ways.

The chosen license of a research software also does not help the designation of
authorship much, as it focuses more on the copyright of the software and does
not provide much information about the authorship.

There also exist some traditions, customs and rules for the author and
non-author contributor identification, but they are unfortunately not presented
in a clear and rigorous written form.

I therefore propose the link:AuthorshipCriteria.adoc[Less Permissive Criteria
for Joint Authorship of Research Software], which aims to provide a set of
less permissive, objective, and consistent criteria to resolve research
software authorship issues.

During the preparation, I have mostly learnt from

* "`A Theory of Joint Authorship for Free and Open Source Software Projects`"
  by Pamela S. Chestek <<Chestek2017>>, in which the author suggests a theory
  of *joint authorship* for the copyright ownership of free and open source
  software projects. Under such a theory, a software becomes *a joint work*
  instead of *a collective work*, which as the author claimed, is a legal
  framework under the U.S. law.
+
I tend to agree with the author and apply the joint authorship for different
research software projects, as I believe it is often the case that a research
software is a unitary whole where individuals' contributions are inseparable or
interdependent.
* "`Guide to the Software Engineering Body of Knowledge, Version 3.0`" edited
  by P. Bourque and R.E. Fairley <<SWEBOKv3>>.
+
I have extensively referred to this book to clearly distinguish different
individuals' intellectual work made for a research software, and what kind of
intellectual work can be used to qualify an individual as a joint author -- I
have named such a work as *softwarization work or contribution*.
* "`Multiple Works (Circ. 34)`" and "`Copyright Registration of Computer
  Programs (Circ. 61)`" published by the U.S. Copyright Office, from which we
  can understand that the U.S. Copyright Office accepts the copyright
  registration of a computer program per version. Multiple versions of the same
  program can be registered if they are not published yet.
+
Therefore, I tend to apply the designation of joint authorship for each verion
(or *a revision* as I have defined in the
link:AuthorshipCriteria.adoc[criteria]) insteaad of a whole development history
of a research software.

NOTE: The copyright registration at the U.S. Copyright Office is not mandatory.
But it seems that a copyright registration is required before a suit for
copyright infringement may be brought, see for example
link:https://corporate.findlaw.com/intellectual-property/how-and-why-to-register-copyrights-for-computer-programs.html[How and Why to Register Copyrights for Computer Programs (accessing date Nov. 25, 2018)].

When determining the joint authorship of a research software, I have referred
to:

* "`Gaiman v. McFarlane: The Right Step in Determining Joint Authorship for
  Copyrighted Material`" by Teresa Huang <<Huang2005>>,

in which the author has, by citing the case of Gaiman v. McFarlane, 360 F.3d
644, 659 (7th Cir. 2004), compared two approaches used for the designation of
joint authorship -- the Nimmer approach and the Goldstein approach.

The author has claimed the preference of the Nimmer approach in the case of
Gaiman v. McFarlane, because this approach "`is fairer to the parties, grants
broader protection, and promotes creativity`" <<Huang2005>>.

The Nimmer approach is also chosen in the proposed
link:AuthorshipCriteria.adoc[criteria]. Under this approach, a person becomes a
joint author a revision of a research software if:

. He/She makes more than a minimal softwarization work thus far in a revision,
  AND
. He/She has the intention that his/her softwarization work be merged into
  inseparable or interdependent parts of the research software as a unitary
  whole.

The left problem is *what is more than a minimal softwarization work thus far
in a revision of a research software?* The original note:

[quote, Melville B. Nimmer and David Nimmer, Nimmer on Copyright § 6.07 (2004)]
[M]ore than a word or line must be added by one who claims to be a joint author

appears to be hard to apply for the determination of more than a minimal
softwarization work. I have therefore referred to <<SWEBOKv3>> to clarify
individuals' softwarization work in different knowledge areas made for a
research software. I have also referred to the following articles and cases to
set up thresholds of sufficient softwarization work in different knowledge
areas:

* "`Perfecting the Pitch: What Constitutes Sufficient Contribution to Give Rise
  to Joint Authorship?`" by A. Antoniou <<Antoniou2018>>, and
* Martin & Anor v Kogan & Ors [2017] EWHC 2927 (IPEC) (22 November 2017) <<EWHC2927>>.

== Annotation

The body of the proposed link:AuthorshipCriteria.adoc[Less Permissive Criteria
for Joint Authorship of Research Software] consists of 4 sections: "`License`",
"`Disclaimer`", "`Definitions`" and "`Criteria`".

In the definition of "`Softwarization work or contribution`", I only include
those in 4 different knowledge areas:

. software requirements,
. software design,
. software construction, and
. software maintenance,

which I believe contribute directly to a research software -- "`[an] expression
of the merged ideas of all the contributors in a fixed, tangible medium`"
<<Huang2005>>.

People can override this definition if they disagree, by providing their own
definition elsewhere; but they must do so also for "`Table 1. Threshold for
softwarization work or contribution more than a minimal.`"

Section 4. "`Criteria`" contains the proposed criteria of addition and removal
of a contributor, which also includes 4 subsections.

Section 4.1. "`Scope`" excludes situations beyond the scope of the proposed
link:AuthorshipCriteria.adoc[criteria]. People must take responsibility on
their own if they want to apply the proposed
link:AuthorshipCriteria.adoc[criteria] in such a situation.

Section 4.2 "`Addition of a contributor`" mostly follows the Nimmer approach,
and the determination of sufficient softwarization work is provided in Section
4.4 "`Sufficient softwarization work`".

Section 4.2 also clearly assigns the identification of a joint
author/non-author contributor of a revision to *recognized joint author(s)* --
those who were already recognized as joint authors before this revision; and in
case that there is any disagreement, "`any decision should be approved by 95%
of all recognized joint authors and no objections from the other 5% in case
that recognized joint author(s)`".

This 95-vs-5 percentage rule is adopted from
link:http://blogs.fsfe.org/ciaran/?p=58[the blog by Ciaran O'Riordan (accessing date Nov. 25, 2018)],
in which the rule can be applied for relicensing software under the GPL. People
can anyhow override this rule when using the proposed
link:AuthorshipCriteria.adoc[criteria] by providing their own rule elsewhere.

There might be case that an individual was forgotten to be identified and added
as a joint author or a non-author contributor of an earlier revision of a
research software. Even though it may be judicial that the designation of joint
authorship or non-author contributorship can date back to this earlier
revision, the implementation of such a procedure is beyond the scope of the
proposed link:AuthorshipCriteria.adoc[criteria]. Instead, I strongly suggest
that people should carefully document any contribution to a research software
and perform such a designation as early as possible.

Section 4.3. "`Removal of a contributor`" clarifies the removal of a non-author
contributor and the removal of a joint author of a research software. The
latter is not feasible under the framework of joint authorship.

An alternative is to create *a derivative work* from a preexisting joint work
(a preexisting research software) in particular when some contributors become
dissatisfied with the development of the research software <<Chestek2017>>.

Whether a new revision created by some contributors can become a derivative
work depends on (i) the intention of these contributors and (ii) the level of
originality of this new revision <<Murray2012>>.

Such a derivative work can become a joint work in case that there are more than
one contributors making softwarization work or contribution, and they follow
the *Intention rule* in Section 4.2 "`Addition of a contributor`" <<Chestek2017>>.

For a derivative work, one should also be aware of that any portion of the
preexisting joint work is still under the copyright protection of the
preexisting joint work; only new and original softwarization work is under the
copyright protection of the derivative work <<Murray2012>>.

Section 4.4 "`Sufficient softwarization work`" establishes the standard of
sufficient softwarization work in a revision of a research software, which
under the Nimmer approach is more than a minimal softwarization work thus far
in the revision.

Following the idea of <<Antoniou2018>> and <<EWHC2927>>, I divide
softwarization work into:

. primary softwarization work (including softwarization work in the software
  design and software construction knowledge areas), and
. secondary softwarization work (including softwarization work in the software
  requirements and software maintenance knowledge areas),

and set up different thresholds for contribution more than a minimal
softwarization work in Table 1.

Such a differentiation of primary and secondary softwarization work is mainly
based on the development of a research software in practice, and it does not
imply that the latter is less important <<Antoniou2018>> <<EWHC2927>>.

The starting point for setting up different thresholds is *non-trivial software
requirements*. My personal philosophy is that any (research) software is made
to solve a practical problem by fulfilling different raised software
requirements. Therefore, we can relate softwarization work in other knowledge
areas to different software requirement(s), and I futher *hypothesize* that any
non-trivial softwarization work should mean to match non-trivial software
requirement(s).

People can for sure disagree with Table 1 and override it by supplying their
own thresholds for the proposed link:AuthorshipCriteria.adoc[criteria] as
regarding the contribution more than a minimal softwarization work.

Furthermore, I leave the determination of:

. what software requirements are non-trivial, and
. what unique activities of the software maintenance are non-trivial,

to recognized joint authors of a research software when applying Table 1 for
the research software. The 95-vs-5 percentage rule during the determination can
also be overridden.

[bibliography]
== References

- [[[Antoniou2018]]] A. Antoniou, Perfecting the Pitch: What Constitutes
  Sufficient Contribution to Give Rise to Joint Authorship? Entertainment and
  Sports Law Journal, 16: 2, pp. 1-4 (2018). DOI: https://doi.org/10.16997/eslj.217
- [[[Chestek2017]]] Pamela S. Chestek, A Theory of Joint Authorship for Free
  and Open Source Software Projects (July 2, 2017). Available at SSRN:
  https://ssrn.com/abstract=2999185 or http://dx.doi.org/10.2139/ssrn.2999185
  (accessing date Nov. 25, 2018).
- [[[EWHC2927]]] Martin & Anor v Kogan & Ors [2017] EWHC 2927 (IPEC) (22
  November 2017). Available at
  http://www.bailii.org/ew/cases/EWHC/IPEC/2017/2927.html (accessing date Nov.
  28, 2018).
- [[[Huang2005]]] Teresa Huang, Gaiman v. McFarlane: The Right Step in
  Determining Joint Authorship for Copyrighted Material, Berkeley Tech. L. J.
  60, 673 (2005). DOI: 10.15779/Z384094.
- [[[ICMJE2017]]] "`Recommendations for the Conduct, Reporting, Editing, and
  Publication of Scholarly Work in Medical Journals`" by International
  Committee of Medical Journal Editors (ICMJE). Available at
  http://www.icmje.org/recommendations (accessing date Nov. 25, 2018).
- [[[Murray2012]]] Brian Murray, Weissmann v. Freeman: The Second Circuit Errs
  in Its Analysis of Derivative Works by Joint Authors, St. John's Law Review:
  Vol. 63: Iss. 3, Article 8 (2012). Available at:
  http://scholarship.law.stjohns.edu/lawreview/vol63/iss3/8
- [[[SWEBOK]]] P. Bourque and R.E. Fairley, eds., Guide to the Software
  Engineering Body of Knowledge, Version 3.0, IEEE Computer Society, 2014;
  www.swebok.org.
